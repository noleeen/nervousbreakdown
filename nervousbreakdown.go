package nervousbreakdown

import "fmt"

type Octopus struct {
	Name  string
	Color string
	Age   int
}

func (o Octopus) Str() string {
	return fmt.Sprintf("The octopus's name %q and it likes %s color", o.Name, o.Color)
}

func Hello() {
	fmt.Println("Alex is good teacher;)")
}
